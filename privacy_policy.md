Attendance Tracker Cloud Privacy Policy
  Effective Date: February 2th, 2017
  
 --- Information gathering and usage ---
When registering for Attendance Tracker Cloud we ask for information such as your name and email address. Your information is only used internally and won't be shared with others.
Attendance Tracker collects the email addresses of those who communicate with us via email, and information submitted through voluntary activities such as site registrations or participation in surveys. Attendance Tracker also collects aggregated, anonymous user data regarding app usage. The user data we collect is never personally identifiable and is used to improve Attendance Tracker and the quality of our service.

 --- Your data ---
Attendance Tracker uses third party vendors and hosting partners to provide the necessary hardware, software, networking, storage, and related technology required to run Attendance Tracker. Although Attendance Tracker owns the code, databases, and all rights to the Attendance Tracker application, you retain all rights to your data. We will never share your personal data with a 3rd party without your prior authorization, and we will never sell data to 3rd parties.

 --- Changes ---
If our information practices change at some time in the future we will post the policy changes to our Web site to notify you of these changes and we will use for these new purposes only data collected from the time of the policy change forward. If you are concerned about how your information is used, you should check back at our Web site periodically.


If you feel that this site is not following its stated information policy, you may contact us at Peterman.apps@gmail.com